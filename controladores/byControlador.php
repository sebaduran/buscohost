<?php
/*controlador de paginas*/
function _homeAction()
{
	//require 'modelos/resources/resourcesModelo.php';
	//Pasa a la vista toda la información que se desea representar
	//$resources = resourceHome($db);
	$home = 'home';
	$title = 'Busca tu mejor alternativa de hosting';

	require 'frontend/header.php';
	require 'vistas/p/index.php';
	require 'frontend/footer.php';
}

function _priceAction()
{
	//require 'modelos/resources/resourcesModelo.php';
	//Pasa a la vista toda la información que se desea representar
	//$resources = resourceHome($db);
	require 'modelos/p/searchModelo.php';
	$home = '0';
	$filter = $_GET['u'];
	$filter = explode("-", $filter);
	$tableInfo = hostingByPrice($db,$filter[0],$filter[1]);
	$title = 'Buscar por precio entre '.$filter[0].'-'.$filter[1];

	require 'frontend/header.php';
	require 'vistas/p/table.php';
	require 'frontend/footer.php';
}

function _spaceAction()
{

	function toByteSize($p_sFormatted) {
	    $aUnits = array('B'=>0, 'KB'=>1, 'MB'=>2, 'GB'=>3, 'TB'=>4, 'PB'=>5, 'EB'=>6, 'ZB'=>7, 'YB'=>8);
	    $sUnit = strtoupper(trim(substr($p_sFormatted, -2)));
	    if (intval($sUnit) !== 0) {
	        $sUnit = 'B';
	    }
	    if (!in_array($sUnit, array_keys($aUnits))) {
	        return false;
	    }
	    $iUnits = trim(substr($p_sFormatted, 0, strlen($p_sFormatted) - 2));
	    if (!intval($iUnits) == $iUnits) {
	        return false;
	    }
	    return $iUnits * pow(1024, $aUnits[$sUnit]);
	}

	require 'modelos/p/searchModelo.php';
	$home = '0';
	$title = 'Buscar por espacio';
	$filter = $_GET['u'];
	$filter = explode("-", $filter);
	$tableInfo = hostingBySpace($db,toByteSize($filter[0]),toByteSize($filter[1]));
	require 'frontend/header.php';
	require 'vistas/p/table.php';
	require 'frontend/footer.php';
}

function _transferAction()
{

	function toByteSize($p_sFormatted) {
	    $aUnits = array('B'=>0, 'KB'=>1, 'MB'=>2, 'GB'=>3, 'TB'=>4, 'PB'=>5, 'EB'=>6, 'ZB'=>7, 'YB'=>8);
	    $sUnit = strtoupper(trim(substr($p_sFormatted, -2)));
	    if (intval($sUnit) !== 0) {
	        $sUnit = 'B';
	    }
	    if (!in_array($sUnit, array_keys($aUnits))) {
	        return false;
	    }
	    $iUnits = trim(substr($p_sFormatted, 0, strlen($p_sFormatted) - 2));
	    if (!intval($iUnits) == $iUnits) {
	        return false;
	    }
	    return $iUnits * pow(1024, $aUnits[$sUnit]);
	}

	require 'modelos/p/searchModelo.php';
	$home = '0';
	$title = 'Buscar por espacio';
	$filter = $_GET['u'];
	$filter = explode("-", $filter);
	$tableInfo = hostingBySpace($db,toByteSize($filter[0]),toByteSize($filter[1]));
	require 'frontend/header.php';
	require 'vistas/p/table.php';
	require 'frontend/footer.php';
}




?>


