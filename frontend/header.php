<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Pragma" content="no-cache">  
  <title>Buscohosting - <? echo $title;?> </title>
  <link rel="stylesheet/less" type="text/css" href="http://www.buscohost.co/frontend/LESS/style.less"/>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://www.buscohost.co/frontend/js/less-1.3.3.min.js"></script>
  <script src="http://www.buscohost.co/frontend/js/prefixfree.min.js"></script>
  <link rel="stylesheet" href="http://www.buscohost.co/resources/demos/style.css" />
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.5.3/modernizr.min.js"></script>
  <script src="http://www.buscohost.co/frontend/js/jquery.raty.js"></script>
  <script src="http://www.buscohost.co/frontend/js/jquery.tablesorter.js"></script>

  <meta property="fb:admins" content="esteldesign"/>
  <meta property="fb:app_id" content="668064009900899"/>
  <meta itemprop="name" content="Buscohost.co - busca la mejor alternativa de hosting para tu proyecto - <? echo $title;?>">
  <meta itemprop="description" content="Busca la mejor alternativa de hosting para tu proyecto">
  <meta name="description" content="Busca la mejor alternativa de hosting para tu proyecto" />
  <meta name="news_keywords" content="Hosting, hostin en chile, el mejor hosting, busco hosting, chile, buscohosting" />
  <meta property="og:site_name" content="Buscohost.co" />

  <meta property="og:image" content="http://www.buscohost.co/frontend/image/facebook.png" />
  <meta property="og:title" content="Buscohost.co" />
  <meta property="og:description" content="Buscohost.co - busca la mejor alternativa de hosting para tu proyecto - <? echo $title;?>" />
  <meta property="og:url" content="<? echo full_url(); ?>" />



<?
$array = array("http://farm4.staticflickr.com/3675/10292628525_779234d261_b.jpg", "http://farm8.staticflickr.com/7400/10292636975_09fde83637_b.jpg", "http://farm6.staticflickr.com/5451/10292643305_c105df8d09_h.jpg", "http://farm9.staticflickr.com/8508/8488136950_ed96b1948a_h.jpg","http://farm9.staticflickr.com/8065/8284559981_be0bf2d4d1_b.jpg","http://farm9.staticflickr.com/8213/8287835656_e16880803b_h.jpg","http://farm3.staticflickr.com/2859/10292629666_102303247f_h.jpg","http://farm4.staticflickr.com/3719/10292519264_2b7e56dfc5_h.jpg","http://farm9.staticflickr.com/8240/8488133950_4e928e3952_h.jpg");
?>
  <script>

  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 3900,
      max: 500000,
      values: [ 15000, 300000 ],
      slide: function( event, ui ) {
        $( "#price" ).val( "$"+ui.values[0]+" - $"+ui.values[1]);
        $("#enviar").attr('href','/by/price/'+ui.values[0]+'-'+ui.values[1]);
      }

    });
    //   $("#slider-range").slider({
    //   range: "min",
    //   value: 30000,
    //   min: 3900,
    //   max: 500000,
    //   slide: function( event, ui ) {
    //   $( "#price" ).val( "$" + ui.value );
    // 	$("#enviar").attr('href','?controlador=by&accion=price&u=3900-'+ui.value);
    //   }
    // });

    $('.popupShare').on('click',function(e){
      e.preventDefault();
      var url =   $(this).attr('href');
      window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=650');
  
    });

    $("table").tablesorter(); 

    $( "#price" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +" - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });
  </script>
</head>
<body class="cf <? echo $home;?>" <? if($home=="home"){?>style="background-image:url(<? echo $array[rand(0,8)]; ?>);"<?} ?>>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=434125903369455";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <header class="span9">
    <div class="inner cf title span6">
      <a href="/"><span class="icon-globe"></span> Buscohost.co <small>beta</small></a>
    </div>
    <nav class="span3 showyourlove">
      <a class="popupShare" href="https://twitter.com/intent/tweet?url=http://www.buscohost.co/&text=Busca tu mejor opcion de hosting en buscohost.co &via=buscohostco"><span class="icon-twitter"></span></a>
      <a class="popupShare" href="http://www.facebook.com/sharer.php?u=http://buscohost.co"><span class="icon-facebook"></span></a>
    </nav>
  </header>