$(document).ready(function(){
	inicial();

	sticky_navigation();
    $(window).scroll(function() {
        sticky_navigation();
    });

    $('form').validate();

    $('.go').click(function(){
		location.href="/p/receta/"+$(this).attr('id')
    })

      $('.rs').click(function(e){
            e.preventDefault();
            var url =   $(this).attr('href');
            window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=650');
        })


});

var inicial = function (){
	$('.slide.parallax').parallax("50%", 0.4);
	$('.category').parent().css('background-position','center center, center bottom');
	$('.category').parent().css('background-size','cover');
	$('.category').parent().css('background-attachment','fixed');


	$(".carrusel_recetas").carouFredSel({
		circular: true,
		infinite: false,
		auto 	: false,
		prev	: {	
			button	: ".recetas.prev",
			key		: "left"
		},
		next	: { 
			button	: ".recetas.next",
			key		: "right"
		}	
	});

	$(".carrusel_jurado").carouFredSel({
		circular: true,
		infinite: false,
		auto 	: false,
		prev	: {	
			button	: ".jurado.prev",
			key		: "left"
		},
		next	: { 
			button	: ".jurado.next",
			key		: "right"
		}	
	});	

	$(".carrusel_premios").carouFredSel({
		circular: true,
		infinite: false,
		auto 	: false,
		prev	: {	
			button	: ".jurado.prev",
			key		: "left"
		},
		next	: { 
			button	: ".jurado.next",
			key		: "right"
		}	
	});


	$(".scroll-section").scroller();

};

var sticky_navigation_offset_top = $('header').offset().top;
 
var sticky_navigation = function(){ 
var scroll_top = $(window).scrollTop(); // our current vertical position from the top

    if (scroll_top > sticky_navigation_offset_top) {
        $('header').addClass('fixed')
    } else {
        $('header').removeClass('fixed')
    }  
};
