<?php
global $servidor, $bd, $usuario, $contrasenia;
$db = new PDO('mysql:host=' . $servidor . ';dbname=' . $bd, $usuario, $contrasenia);

function hostingByPrice($db,$min,$max)
{
	$consulta = $db->prepare("SELECT * FROM plains JOIN enterprise ON plains.`plain-parent-id` = enterprise.`enterprise-id` WHERE `plain-price` BETWEEN ".$min." AND ".$max." ORDER BY `plains`.`plain-price` ASC");
	$consulta->execute();
	return $consulta->fetchAll();
}

function hostingBySpace($db,$min,$max)
{
	$consulta = $db->prepare("SELECT * FROM plains JOIN enterprise ON plains.`plain-parent-id` = enterprise.`enterprise-id` WHERE  `plain-space-bytes` BETWEEN ".$min." AND ".$max." ORDER BY  `plains`.`plain-space-bytes` ASC ");
	$consulta->execute();
	return $consulta->fetchAll();
}

?>

