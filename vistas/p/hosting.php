<? foreach($hosting as $hosting){?>
<section class="container hostingInfo" style="margin-top:40px;">
	<h1><a href="<? echo $hosting['enterprise-url']; ?>" target="_blank"><? echo $hosting['enterprise-name']; ?></a></h1>
	<h4><img src="http://www.buscohost.co/frontend/image/banderas/<? echo $hosting['enterprise-country']; ?>.png"/> <a href="<? echo $hosting['enterprise-url']; ?>" target="_blank"><? echo $hosting['enterprise-url']; ?></a> <? if($hosting['enterprise-twitter']!=""){?>· <a href="http://www.twitter.com/<? echo $hosting['enterprise-twitter']; ?>"><span class="icon-twitter"></span> Twitter</a> <?}?><? if($hosting['enterprise-facebook']!=""){?>· <a href="http://www.facebook.com/<? echo $hosting['enterprise-facebook']; ?>"><span class="icon-facebook"></span> Facebook</a><?}?></h4>
	<div class="span7">

		<div class="span2">
			<figure>
				<img src="<? echo $hosting['enterprise-logo']; ?>"/>
			</figure>
		</div>
		<div class="span7 col">
			<p>
			<? if($hosting['enterprise-description']!=""){?>
				<? echo utf8_encode($hosting['enterprise-description']); ?>
			<?}else{?>
				<center><strong>El sitio web de esta empresa no tiene descripcion de la empresa</strong><br/><span style="font-size:40px;" class="icon-sad"></span></center>
			<?}?>
			</p>
		</div>
		<div class="span9 comments">
			<h3><span class="icon-bubble"></span> Tu comentario ayudará a otros usuarios a elegir bien su proveedor de hosting</h3>
			<div class="fb-comments" data-href="<? echo full_url(); ?>" data-colorscheme="light" data-numposts="20" data-width="800"></div>
		</div>

	</div>
<?}?>
	<aside class="span2 col" style="float:right;">
		<h2><span class="icon-folder"></span> Planes</h2>
		<? foreach($planes as $planes){?>
		<article class="asidePlans">
			<h3><? echo utf8_encode($planes['plain-name']); ?></h3>
			<h4>$<? echo number_format($planes['plain-price'],0,'','.') ?></h4>
			<div class="star" data-number="5" data-score="3"></div>
			<a href="<? echo $planes['plain-url']; ?>" target="_blank">ver más</a>
		</article>
		<?}?>

	</aside>


<script>
$('.fb-comments').attr('data-width',$('.comments').css('width'));
$( window ).resize(function() {
	$('.fb-comments').attr('data-width',$('.comments').css('width'));
});
$('.star').raty({
	path: 'http://www.buscohost.co/frontend/image',
  	number: function() {
    	return $(this).attr('data-number');
  	},
  	score: function() {
    	return $(this).attr('data-score');
  	}

});



</script>

</section>