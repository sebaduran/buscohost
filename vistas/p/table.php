<section>
	<div class="slideDown">	 
		<section class="container">
			<h1>¿Hasta cuanto quieres gastar?</h1>
			<center>
				<input type="text" id="price" value="-" style="border: 0; color: #f6931f; font-weight: bold;" />
				<div id="slider-range" style="width:500px;"></div>
				<a href="#" id="enviar" class="boton-enviar">Buscar</a>
			</center>
			<div class="slideDownTrigger">
			Buscar <span class="icon-zoom-in"></span>
			</div>
		</section>
	</div>
</section>

<section class="container principalContent"><!-- container -->

<h2><span class="icon-earth"></span> hostings encontrado: <? echo count($tableInfo);?></h2>

<?
function bytes2English($filesize)
    {
    if ($filesize<1048676)
        RETURN number_format($filesize/1024,1) . " KB";
    if ($filesize>=1048576 && $filesize<1073741824)
        RETURN number_format($filesize/1048576,1) . " MB";
    if ($filesize>=1073741824 && $filesize<1099511627776)
        RETURN number_format($filesize/1073741824,2) . " GB";
    if ($filesize>=1099511627776)
        RETURN number_format($filesize/1099511627776,2) . " TB";
    if ($filesize>=1125899906842624) //Currently, PB won't show due to PHP limitations
        RETURN number_format($filesize/1125899906842624,3) . " PB";
    }
?>
	<table class="span9" cellspacing="0">
		<thead>
			<tr>
				<th style="text-align:left;">Plan</th>
				<th>País</th>		
				<th>Espacio</th>		
				<th>Transferencia</th>
				<th>Correo</th>
				<th>Bases de dato</th>
				<th>Rating</th>
				<th>Precio</th>
			</tr>
		</thead>
		<tbody>
			<? foreach($tableInfo AS $tableInfo){?>
			<tr>
				<th class="enterprice"><a class="plan" target="_blank" href="<? echo $tableInfo['plain-url']; ?>"><? echo utf8_encode($tableInfo['plain-name']); ?></a></br><a href="/hosting/company/<? echo $tableInfo['enterprise-slug']; ?>"><small><? echo $tableInfo['enterprise-name']; ?></small></a></th>
				
				<th>
				<? if($tableInfo['plain-datacenter']!="-"){?>
					<img src="http://www.buscohost.co/frontend/image/banderas/<? echo $tableInfo['plain-datacenter']; ?>.png"/>
				<? }else{ ?>
					-
				<? } ?>
				</th>	
				<? if($_GET['accion']=="space"){ $class="select";}?>
				<?if($tableInfo['plain-space']==0){?>	
				<th class="<? echo $class;?>"><? echo 'Ilimitado'; ?></th>
				<?}else{?>
				<th class="<? echo $class;?>"><? echo $tableInfo['plain-space']; ?> <? echo $tableInfo['plain-space-unit']; ?></th>
				<?}?>
				<? if($tableInfo['plain-transfer']==1){?>
				<th><? echo 'Ilimitado'; ?></th>
				<?}else{?>
				<th><? echo bytes2English($tableInfo['plain-transfer']); ?></th>
				<?}?>
				<? if($tableInfo['plain-email-account']==0){?>
				<th><? echo 'Ilimitado'; ?></th>
				<?}else{?>
				<th><? echo $tableInfo['plain-email-account']; ?></th>
				<?}?>
				<? if($tableInfo['plain-mysql-quantity']==11235813){?>
				<th><? echo 'Ilimitado'; ?></th>
				<?}else{?>
				<th><? echo $tableInfo['plain-mysql-quantity']; ?></th>
				<?}?>
				<th><div class="star"  data-id="<? echo $tableInfo['plain-id']; ?>" data-number="5" data-score="<? echo $tableInfo['plain-calification']; ?>" data-users="<? echo $tableInfo['plain-calification-users']; ?>"></div></th>
				<? if($_GET['accion']=="price"){ $class1="select";}?>
				<th class="price <? echo $class1;?>">$ <? echo number_format($tableInfo['plain-price'],0,'','.'); ?></th>
			</tr>
			<?}?>
		</tbody>
	</table>
</section>

<section class="faqs span9">
	<section class="container" >
		<h2>Cosas que necesitas saber</h2>
		<article class="span3">
			<center class="ico"><span class="icon-transfer"></span></center>
			<h3>Transferencia</h3>
			Es la cantidad de informacion que puede transferir desde y hacia el servidor (mover archivos por ftp, visitando la pagina web, enviando y recibiendo correos).
		</article>

		<article class="span3 col">
			<center class="ico"><span class="icon-upload"></span></center>
			<h3>FTP</h3>
			El FTP es la herramienta que le permite, a traves de la red, copiar ficheros de un ordenador a otro. Y ello, sin importar en absoluto donde estan localizados estos ordenadores,  o si es que usan el mismo sistema operativo: basta con que esten conectados a Internet.
		</article>

		<article class="span3 col">
			<center class="ico"><span class="icon-folder-open"></span></center>
			<h3>Hosting</h3>
			El alojamiento web (en inglés web hosting) es el servicio que provee a los usuarios de Internet un sistema para poder almacenar información, imágenes, vídeo, o cualquier contenido accesible vía web.
		</article>

		<article class="span3">
			<center class="ico"><span class=" icon-server"></span></center>
			<h3>Bases de datos</h3>
			Una base de datos o banco de datos es un conjunto de datos pertenecientes a un mismo contexto y almacenados sistemáticamente para su posterior uso.
		</article>
	</section>
	<!--<section class="container" >
		<h2>Unidades de espacio</h2>
		<article class="span3">
			<h3>Byte</h3>
			es la unidad fundamental de datos en los ordenadores personales, un byte son ocho bits contiguos. El byte es también la unidad de medida básica para memoria, almacenando el equivalente a un carácter.
		</article>

		<article class="span3 col">
		<h3>Kilobyte</h3>
		(abreviado como KB o Kbyte) es una unidad de medida equivalente a mil bytes de memoria de ordenador o de capacidad de disco. Por ejemplo, un dispositivo que tiene 256K de memoria puede almacenar aproximadamente 256.000 bytes (o caracteres) de una vez.
		</article>

		<article class="span3 col">
		<h3>Megabyte</h3>
		(MB) es una unidad de medida de cantidad de datos informáticos. Es un múltiplo del byte u octeto, que equivale a 106 bytes.
		</article>

		<article class="span3 ">
		<h3>Gigabyte</h3>
		es una unidad de medida informática cuyo símbolo es el GB, y puede equivalerse a 230 bytes o a 109 bytes, según el uso.
		</article>

		<article class="span3 col">
		<h3>Terabyte</h3>
		es una unidad de medida de almacenamiento de datos cuyo símbolo es TB y equivale a 1000 GB.
		</article>
	</section>-->
</section>

<script>
function localStorageRead(){
localStorage.getItem('listado')
}

$('.star').raty({
	path: 'http://www.buscohost.co/frontend/image',
  	number: function() {
    	return $(this).attr('data-number');
  	},
  	score: function() {
    	return $(this).attr('data-score');
  	},
  	click: function(score, evt) {
    	//alert('ID: ' + $(this).attr('data-id') + "\nscore: " + score + "\nusers: " + $(this).attr('data-users'));
    	newScore = (parseInt($(this).attr('data-score'))+parseInt(score))/parseInt($(this).attr('data-users'));
    	if(localStorage.getItem($(this).attr('data-id'))=="1"){
    		readOnly($(this).attr('data-id'));
    		alert('Ya habias votado... ¿lo recuerdas?');
    	}
	  	$.post( "?controlador=interaction&accion=rating", { id:$(this).attr('data-id'), score: newScore, users: parseInt($(this).attr('data-users'))+1})
		  .success(function( data ) {
		  	var data = data.replace(/\s/g,'');
		  	var data = data.split('-');
		    if(data[0]=="1"){
		    	$('[data-id='+data[1]+']').attr('data-score',data[2])
		    	readOnly(data[1]);

		    }else{
		    	console.log('no vote')
		    }
		  });

  	}
});

function readOnly(id){
	$('[data-id='+id+']').raty({
		path: 'http://www.buscohost.co/frontend/image',
	  	readOnly: true, 
	  	score: function(){
	  		return $(this).attr('data-score');
	  	}
	})
	localStorage.setItem(id,'1');
}

$('.slideDownTrigger').click(function(){
	$('.slideDown').toggleClass('down');
})

</script>


