<section class="container" style="margin-top:40px;">
	<h1><span class="icon-cool"></span> Transparencia</h1>
	<p class="regular">Para dejar las cosas claras, <a href="http://www.buscohost.co">buscohost.co</a>  no pertenece a ninguna empresa de hosting, estamos alojados en benzahost pero no pertenecemos ni a esa empresa ni a ninguna otra..</p>
	<p class="regular">El espíritu de este sitio es lograr transparentar los precios de hosting en el mercado y entregar comparativamente las especificaciones de cada uno para así ayudar a tomar una buena desisión. </p>
	<p class="regular">Cualquier duda, consulta, apoyo o lo que sea no duden en escribír a <a href="mailto:to@sebaduran.com?subject=[Buscohost.co]">yo[at]sebaduran[dot]com</a> y encantado les responderé. </p><br/>
	<p class="regular">pd: las imagenes en el home son de <a href="http://www.flickr.com/photos/sebaduran/" target="_blank">flickr</a></p>
</section> 

